export class DeathSaves {
  static init() {
    console.log("DeathSaves Hi from init.");

    Hooks.on("updateCombat", (document, change, options, userId) => {
      console.log("Death Saves hook " + document.constructor.name + ":\n" + JSON.stringify(document));
      if (game.combat.started)
      {
        let currentCombatant = _getCurrentCombatActor(document._id);
        
        if ((currentCombatant.attributes.hp.value === 0) &&
            (currentCombatant.attributes.death.failure !==3) &&
            (currentCombatant.attributes.death.success !==3) /*&&
          (currentCombatant.effects.find((v) => value === "unconscous"))*/){
          console.log("sending chat message");
          let who = game.users.filter((u) => u.isGM ).concat(game.combat.combatant.players).map((u) => u.id);
          const chatData = {
            user: game.user._id,
            speaker: game.user,
            content: game.combat.combatant.actor.name + " needs to roll a death save",
            whisper: who,
          };
          ChatMessage.create(chatData, {});
        }
      }
    });
  
    Hooks.on("preUpdateCombatTracker", (document, change, options, userId) => {
      console.log("Death Saves hook " + document.constructor.name + ":\n" + JSON.stringify(document));
    });
  
    Hooks.on("preUpdateActor", (document, change, options, userId) => {
      console.log("Death Saves hook " + document.constructor.name + ":\n" + JSON.stringify(document));
    });

    Hooks.on("init", (document, change, options, userId) => {
      console.log("Death Saves hook init");
    });

    Hooks.on("ready", (document, change, options, userId) => {
      console.log("Death Saves hook ready 1");
    });
    
    function _getCurrentCombatActor(id) {
      return game.combat.combatant.actor.data.data;
    }
  }
}
